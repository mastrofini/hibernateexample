package it.uniroma2.isssr.examples;

import javax.persistence.*;

/**
 * Created by mastro on 05/03/17.
 */
@Entity
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    protected Item() {

    }

    public Item(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Item " + id + " - " + name;
    }

}
