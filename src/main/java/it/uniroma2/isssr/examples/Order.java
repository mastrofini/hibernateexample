package it.uniroma2.isssr.examples;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mastro on 05/03/17.
 */
@Entity
@Table(name = "OrderT")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    private long creationTimestamp;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Item> items;

    public Order() {
        this.creationTimestamp = System.currentTimeMillis();
        items = new LinkedList<>();
    }

    public void addItem(Item i) {
        items.add(i);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Order ").append(id).append(":\n");

        String itemString = items.stream().map(Object::toString)
                .collect(Collectors.joining("\n"));

        sb.append(itemString);
        sb.append("\n");
        return sb.toString();
    }

    public void removeItem(int i) {
        if (i < items.size())
            items.remove(i);
    }
}
