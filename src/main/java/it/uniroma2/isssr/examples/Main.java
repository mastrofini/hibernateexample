package it.uniroma2.isssr.examples;

import org.hibernate.HibernateException;
import org.hibernate.Metamodel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by mastro on 05/03/17.
 */
public class Main {
    private static final SessionFactory ourSessionFactory;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure()
                    .addAnnotatedClass(Item.class)
                    .addAnnotatedClass(Order.class)
                    .addAnnotatedClass(SpecialOrder.class)
                    .addAnnotatedClass(CopyOfSpecialOrder.class);

            ourSessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    public static void main(final String[] args) throws Exception {

        // creazione e salvataggio di un ordine
        final Session sessionSalvataggio = getSession();
        Order o = new Order();
        Item item1 = new Item("Test");
        o.addItem(item1);
        Item item2 = new Item("Test2");
        o.addItem(item2);

        sessionSalvataggio.beginTransaction();
        sessionSalvataggio.save(o);
        sessionSalvataggio.getTransaction().commit();
        sessionSalvataggio.close();

        // lettura dell'ordine dal database
        readAndPrintOrders();

        // cancellazione di un oggetto dall'ordine
        final Session sessionCancellazione = getSession();
        Query queryLetturaECancellazione = sessionCancellazione.createQuery("from Order");
        sessionCancellazione.beginTransaction();
        Order ordineDalQualeCancellare = sessionCancellazione.find(Order.class, 1L);
        ordineDalQualeCancellare.removeItem(0);
        sessionCancellazione.getTransaction().commit();
        sessionCancellazione.close();

        // lettura del nuovo ordine senza oggetto 1
        readAndPrintOrders();


        // aggiunta di un oggetto all'ordine
        final Session sessionUpdate = getSession();
        Query queryLetturaEAggiornamento = sessionUpdate.createQuery("from Order");
        sessionUpdate.beginTransaction();
        Order ordineDaAggiornare = sessionUpdate.find(Order.class, 1L);
        Item item3 = new Item("Test 3");
        ordineDaAggiornare.addItem(item3);
        sessionUpdate.getTransaction().commit();
        sessionUpdate.close();

        // lettura del nuovo ordine con nuovo oggetto 3
        readAndPrintOrders();

        ourSessionFactory.close();
    }

    private static void readAndPrintOrders() {
        final Session sessionSecondaLettura = getSession();
        Query querySecondaLetturaOrdini = sessionSecondaLettura.createQuery("from Order");
        List<Order> listaOrdini = querySecondaLetturaOrdini.getResultList();
        listaOrdini.forEach(System.out::println);
        sessionSecondaLettura.close();
    }
}